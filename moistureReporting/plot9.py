from  matplotlib.backends.backend_qt5agg  import  ( 
        FigureCanvas ,  NavigationToolbar2QT  as  NavigationToolbar )

from  matplotlib.figure  import  Figure
import matplotlib.pyplot as plt

from PySide2 import QtCore, QtGui, QtWidgets
from  PySide2.QtCore  import  QTime
from persiantools.jdatetime import JalaliDateTime

import datetime
import psycopg2

sql_user = "postgres"
sql_pass = "somepass" 
sql_host = "localhost"
sql_port = "5432"
db_name = "moisturemeter"

num_of_ticks = 8

# ------------------ MplWidget ------------------ 
class  MplWidget ( QtWidgets.QWidget ):
    
    def  __init__ ( self ,  parent  =  None ):
        
        QtWidgets.QWidget . __init__ ( self ,  parent )
        
        self.figure = plt.figure() 
        self . canvas  =  FigureCanvas ( self.figure )
        
        vertical_layout  =  QtWidgets.QVBoxLayout () 
        vertical_layout . addWidget ( self . canvas ) 
        vertical_layout . addWidget ( NavigationToolbar ( self . canvas ,  self ))
        
        self . canvas . axes  =  self . canvas . figure . add_subplot ( 111 ) 
        self . setLayout ( vertical_layout )   


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(900, 826)
        MainWindow.setWindowIcon(QtGui.QIcon('Required_files/chart.png'))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.line_2 = QtWidgets.QFrame(self.centralwidget)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.verticalLayout.addWidget(self.line_2)
        self.gridLayout_3 = QtWidgets.QGridLayout()
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setMaximumSize(QtCore.QSize(90, 16777215))
        font = QtGui.QFont()
        font.setFamily("B Nazanin")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.gridLayout_3.addWidget(self.label_2, 2, 12, 1, 1)
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setMaximumSize(QtCore.QSize(8, 16777215))
        self.label_5.setObjectName("label_5")
        self.gridLayout_3.addWidget(self.label_5, 1, 10, 1, 1)
        self.comboBox_month_to = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox_month_to.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setFamily("B Nazanin")
        font.setPointSize(12)
        self.comboBox_month_to.setFont(font)
        self.comboBox_month_to.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.comboBox_month_to.setMaxVisibleItems(10)
        self.comboBox_month_to.setDuplicatesEnabled(False)
        self.comboBox_month_to.setObjectName("comboBox_month_to")
        self.comboBox_month_to.addItem("")
        self.comboBox_month_to.addItem("")
        self.comboBox_month_to.addItem("")
        self.comboBox_month_to.addItem("")
        self.comboBox_month_to.addItem("")
        self.comboBox_month_to.addItem("")
        self.comboBox_month_to.addItem("")
        self.comboBox_month_to.addItem("")
        self.comboBox_month_to.addItem("")
        self.comboBox_month_to.addItem("")
        self.comboBox_month_to.addItem("")
        self.comboBox_month_to.addItem("")
        self.gridLayout_3.addWidget(self.comboBox_month_to, 2, 9, 1, 1)
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setMaximumSize(QtCore.QSize(10, 16777215))
        self.label_6.setText("")
        self.label_6.setObjectName("label_6")
        self.gridLayout_3.addWidget(self.label_6, 2, 6, 1, 1)
        self.spinBox_day_to = QtWidgets.QSpinBox(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily("B Nazanin")
        font.setPointSize(12)
        font.setBold(False)
        font.setWeight(50)
        self.spinBox_day_to.setFont(font)
        self.spinBox_day_to.setMinimum(1)
        self.spinBox_day_to.setMaximum(31)
        self.spinBox_day_to.setObjectName("spinBox_day_to")
        self.gridLayout_3.addWidget(self.spinBox_day_to, 2, 11, 1, 1)
        self.comboBox_year_from = QtWidgets.QComboBox(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily("B Nazanin")
        font.setPointSize(12)
        self.comboBox_year_from.setFont(font)
        self.comboBox_year_from.setObjectName("comboBox_year_from")
        self.gridLayout_3.addWidget(self.comboBox_year_from, 1, 7, 1, 1)
        self.label_14 = QtWidgets.QLabel(self.centralwidget)
        self.label_14.setText("")
        self.label_14.setObjectName("label_14")
        self.gridLayout_3.addWidget(self.label_14, 2, 13, 1, 1)
        self.comboBox_month_from = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox_month_from.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setFamily("B Nazanin")
        font.setPointSize(12)
        self.comboBox_month_from.setFont(font)
        self.comboBox_month_from.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.comboBox_month_from.setMaxVisibleItems(10)
        self.comboBox_month_from.setObjectName("comboBox_month_from")
        self.comboBox_month_from.addItem("")
        self.comboBox_month_from.addItem("")
        self.comboBox_month_from.addItem("")
        self.comboBox_month_from.addItem("")
        self.comboBox_month_from.addItem("")
        self.comboBox_month_from.addItem("")
        self.comboBox_month_from.addItem("")
        self.comboBox_month_from.addItem("")
        self.comboBox_month_from.addItem("")
        self.comboBox_month_from.addItem("")
        self.comboBox_month_from.addItem("")
        self.comboBox_month_from.addItem("")
        self.gridLayout_3.addWidget(self.comboBox_month_from, 1, 9, 1, 1)
        self.label_11 = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily("B Nazanin")
        font.setPointSize(12)
        font.setBold(False)
        font.setWeight(50)
        self.label_11.setFont(font)
        self.label_11.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label_11.setAlignment(QtCore.Qt.AlignCenter)
        self.label_11.setObjectName("label_11")
        self.gridLayout_3.addWidget(self.label_11, 0, 9, 1, 1)
        self.label_16 = QtWidgets.QLabel(self.centralwidget)
        self.label_16.setText("")
        self.label_16.setObjectName("label_16")
        self.gridLayout_3.addWidget(self.label_16, 1, 0, 1, 1)
        self.label_18 = QtWidgets.QLabel(self.centralwidget)
        self.label_18.setMaximumSize(QtCore.QSize(80, 16777215))
        font = QtGui.QFont()
        font.setFamily("B Nazanin")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.label_18.setFont(font)
        self.label_18.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label_18.setAlignment(QtCore.Qt.AlignCenter)
        self.label_18.setObjectName("label_18")
        self.gridLayout_3.addWidget(self.label_18, 1, 2, 1, 1)
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setMaximumSize(QtCore.QSize(90, 16777215))
        font = QtGui.QFont()
        font.setFamily("B Nazanin")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.gridLayout_3.addWidget(self.label, 1, 12, 1, 1)
        self.label_10 = QtWidgets.QLabel(self.centralwidget)
        self.label_10.setMaximumSize(QtCore.QSize(16777215, 41))
        font = QtGui.QFont()
        font.setFamily("B Nazanin")
        font.setPointSize(12)
        font.setBold(False)
        font.setWeight(50)
        self.label_10.setFont(font)
        self.label_10.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label_10.setAlignment(QtCore.Qt.AlignCenter)
        self.label_10.setObjectName("label_10")
        self.gridLayout_3.addWidget(self.label_10, 0, 11, 1, 1)
        self.pushButton_gen_plot = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_gen_plot.setEnabled(True)
        self.pushButton_gen_plot.setMinimumSize(QtCore.QSize(150, 40))
        font = QtGui.QFont()
        font.setFamily("B Nazanin")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.pushButton_gen_plot.setFont(font)
        self.pushButton_gen_plot.setAutoDefault(True)
        self.pushButton_gen_plot.setDefault(False)
        self.pushButton_gen_plot.setFlat(False)
        self.pushButton_gen_plot.setObjectName("pushButton_gen_plot")
        self.gridLayout_3.addWidget(self.pushButton_gen_plot, 2, 1, 1, 1)
        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setMaximumSize(QtCore.QSize(8, 16777215))
        self.label_7.setObjectName("label_7")
        self.gridLayout_3.addWidget(self.label_7, 2, 8, 1, 1)
        self.spinBox_day_from = QtWidgets.QSpinBox(self.centralwidget)
        self.spinBox_day_from.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setFamily("B Nazanin")
        font.setPointSize(12)
        font.setBold(False)
        font.setWeight(50)
        self.spinBox_day_from.setFont(font)
        self.spinBox_day_from.setMinimum(1)
        self.spinBox_day_from.setMaximum(31)
        self.spinBox_day_from.setObjectName("spinBox_day_from")
        self.gridLayout_3.addWidget(self.spinBox_day_from, 1, 11, 1, 1)
        self.label_13 = QtWidgets.QLabel(self.centralwidget)
        self.label_13.setMaximumSize(QtCore.QSize(12, 16777215))
        self.label_13.setText("")
        self.label_13.setObjectName("label_13")
        self.gridLayout_3.addWidget(self.label_13, 1, 4, 1, 1)
        self.label_8 = QtWidgets.QLabel(self.centralwidget)
        self.label_8.setMaximumSize(QtCore.QSize(8, 16777215))
        self.label_8.setObjectName("label_8")
        self.gridLayout_3.addWidget(self.label_8, 2, 10, 1, 1)
        self.comboBox_year_to = QtWidgets.QComboBox(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily("B Nazanin")
        font.setPointSize(12)
        self.comboBox_year_to.setFont(font)
        self.comboBox_year_to.setObjectName("comboBox_year_to")
        self.gridLayout_3.addWidget(self.comboBox_year_to, 2, 7, 1, 1)
        self.label_12 = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily("B Nazanin")
        font.setPointSize(12)
        font.setBold(False)
        font.setWeight(50)
        self.label_12.setFont(font)
        self.label_12.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label_12.setAlignment(QtCore.Qt.AlignCenter)
        self.label_12.setObjectName("label_12")
        self.gridLayout_3.addWidget(self.label_12, 0, 7, 1, 1)
        self.comboBox_sheet_coeficiant = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox_sheet_coeficiant.setMinimumSize(QtCore.QSize(80, 0))
        font = QtGui.QFont()
        font.setFamily("B Nazanin")
        font.setPointSize(12)
        self.comboBox_sheet_coeficiant.setFont(font)
        self.comboBox_sheet_coeficiant.setObjectName("comboBox_sheet_coeficiant")
        self.comboBox_sheet_coeficiant.addItem("")
        self.comboBox_sheet_coeficiant.addItem("")
        self.comboBox_sheet_coeficiant.addItem("")
        self.gridLayout_3.addWidget(self.comboBox_sheet_coeficiant, 1, 1, 1, 1)
        self.label_9 = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily("B Nazanin")
        font.setPointSize(12)
        font.setBold(False)
        font.setWeight(50)
        self.label_9.setFont(font)
        self.label_9.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label_9.setAlignment(QtCore.Qt.AlignCenter)
        self.label_9.setObjectName("label_9")
        self.gridLayout_3.addWidget(self.label_9, 0, 5, 1, 1)
        self.timeEdit_from = QtWidgets.QTimeEdit(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily("B Nazanin")
        font.setPointSize(12)
        font.setBold(False)
        font.setWeight(50)
        self.timeEdit_from.setFont(font)
        self.timeEdit_from.setTimeSpec(QtCore.Qt.LocalTime)
        self.timeEdit_from.setTime(QtCore.QTime(0, 0, 0))
        self.timeEdit_from.setObjectName("timeEdit_from")
        self.gridLayout_3.addWidget(self.timeEdit_from, 1, 5, 1, 1)
        self.timeEdit_to = QtWidgets.QTimeEdit(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily("B Nazanin")
        font.setPointSize(12)
        font.setBold(False)
        font.setWeight(50)
        self.timeEdit_to.setFont(font)
        self.timeEdit_to.setObjectName("timeEdit_to")
        self.gridLayout_3.addWidget(self.timeEdit_to, 2, 5, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setMaximumSize(QtCore.QSize(10, 16777215))
        self.label_3.setText("")
        self.label_3.setObjectName("label_3")
        self.gridLayout_3.addWidget(self.label_3, 1, 6, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setMaximumSize(QtCore.QSize(8, 16777215))
        self.label_4.setObjectName("label_4")
        self.gridLayout_3.addWidget(self.label_4, 1, 8, 1, 1)
        self.checkBox_max = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBox_max.setMaximumSize(QtCore.QSize(110, 16777215))
        font = QtGui.QFont()
        font.setFamily("B Nazanin")
        font.setPointSize(12)
        self.checkBox_max.setFont(font)
        self.checkBox_max.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.checkBox_max.setChecked(True)
        self.checkBox_max.setObjectName("checkBox_max")
        self.gridLayout_3.addWidget(self.checkBox_max, 1, 3, 1, 1)
        self.checkBox_min = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBox_min.setMinimumSize(QtCore.QSize(90, 0))
        self.checkBox_min.setMaximumSize(QtCore.QSize(110, 16777215))
        font = QtGui.QFont()
        font.setFamily("B Nazanin")
        font.setPointSize(12)
        self.checkBox_min.setFont(font)
        self.checkBox_min.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.checkBox_min.setObjectName("checkBox_min")
        self.gridLayout_3.addWidget(self.checkBox_min, 2, 3, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout_3)
        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout.addWidget(self.line)
        self.MplWidget = MplWidget(self.centralwidget)
        self.MplWidget.setMinimumSize(QtCore.QSize(700, 600))
        self.MplWidget.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.MplWidget.setObjectName("MplWidget")
        self.verticalLayout.addWidget(self.MplWidget)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 900, 26))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        # added by me
        self.pushButton_gen_plot.clicked.connect(self.update_graph)
        self.fill_year_combo()
        self.timeEdit_from.setTime(QTime.currentTime())
        self.timeEdit_to.setTime(QTime.currentTime())
        self.checkBox_min.stateChanged.connect(self.draw_plot)
        self.checkBox_max.stateChanged.connect(self.draw_plot)
        
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        MainWindow.setTabOrder(self.spinBox_day_from, self.comboBox_month_from)
        MainWindow.setTabOrder(self.comboBox_month_from, self.comboBox_year_from)
        MainWindow.setTabOrder(self.comboBox_year_from, self.timeEdit_from)
        MainWindow.setTabOrder(self.timeEdit_from, self.spinBox_day_to)
        MainWindow.setTabOrder(self.spinBox_day_to, self.comboBox_month_to)
        MainWindow.setTabOrder(self.comboBox_month_to, self.comboBox_year_to)
        MainWindow.setTabOrder(self.comboBox_year_to, self.timeEdit_to)
        MainWindow.setTabOrder(self.timeEdit_to, self.checkBox_max)
        MainWindow.setTabOrder(self.checkBox_max, self.checkBox_min)
        MainWindow.setTabOrder(self.checkBox_min, self.comboBox_sheet_coeficiant)
        MainWindow.setTabOrder(self.comboBox_sheet_coeficiant, self.pushButton_gen_plot)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "نمودار مینیمم/ماکسیمم رطوبت کل"))
        self.label_10.setText(_translate("MainWindow", "روز"))
        self.label_8.setText(_translate("MainWindow", "/"))
        self.label_9.setText(_translate("MainWindow", "ساعت"))
        self.timeEdit_to.setDisplayFormat(_translate("MainWindow", "hh:mm"))
        self.pushButton_gen_plot.setText(_translate("MainWindow", "نمایش نمودار"))
        self.label_18.setText(_translate("MainWindow", "ضخامت:"))
        self.comboBox_sheet_coeficiant.setItemText(0, _translate("MainWindow", "همه ضخامت ها"))
        self.comboBox_sheet_coeficiant.setItemText(1, _translate("MainWindow", "8"))
        self.comboBox_sheet_coeficiant.setItemText(2, _translate("MainWindow", "10"))
        self.comboBox_sheet_coeficiant.setItemText(3, _translate("MainWindow", "12"))
        self.comboBox_month_from.setItemText(0, _translate("MainWindow", "1"))
        self.comboBox_month_from.setItemText(1, _translate("MainWindow", "2"))
        self.comboBox_month_from.setItemText(2, _translate("MainWindow", "3"))
        self.comboBox_month_from.setItemText(3, _translate("MainWindow", "4"))
        self.comboBox_month_from.setItemText(4, _translate("MainWindow", "5"))
        self.comboBox_month_from.setItemText(5, _translate("MainWindow", "6"))
        self.comboBox_month_from.setItemText(6, _translate("MainWindow", "7"))
        self.comboBox_month_from.setItemText(7, _translate("MainWindow", "8"))
        self.comboBox_month_from.setItemText(8, _translate("MainWindow", "9"))
        self.comboBox_month_from.setItemText(9, _translate("MainWindow", "10"))
        self.comboBox_month_from.setItemText(10, _translate("MainWindow", "11"))
        self.comboBox_month_from.setItemText(11, _translate("MainWindow", "12"))
        self.label_7.setText(_translate("MainWindow", "/"))
        self.timeEdit_from.setDisplayFormat(_translate("MainWindow", "hh:mm"))
        self.label.setText(_translate("MainWindow", "  از تاریخ:  "))
        self.label_12.setText(_translate("MainWindow", "سال"))
        self.label_11.setText(_translate("MainWindow", "ماه"))
        self.label_2.setText(_translate("MainWindow", "  تا تاریخ:   "))
        self.label_5.setText(_translate("MainWindow", "/"))
        self.label_4.setText(_translate("MainWindow", "/"))
        self.comboBox_month_to.setItemText(0, _translate("MainWindow", "1"))
        self.comboBox_month_to.setItemText(1, _translate("MainWindow", "2"))
        self.comboBox_month_to.setItemText(2, _translate("MainWindow", "3"))
        self.comboBox_month_to.setItemText(3, _translate("MainWindow", "4"))
        self.comboBox_month_to.setItemText(4, _translate("MainWindow", "5"))
        self.comboBox_month_to.setItemText(5, _translate("MainWindow", "6"))
        self.comboBox_month_to.setItemText(6, _translate("MainWindow", "7"))
        self.comboBox_month_to.setItemText(7, _translate("MainWindow", "8"))
        self.comboBox_month_to.setItemText(8, _translate("MainWindow", "9"))
        self.comboBox_month_to.setItemText(9, _translate("MainWindow", "10"))
        self.comboBox_month_to.setItemText(10, _translate("MainWindow", "11"))
        self.comboBox_month_to.setItemText(11, _translate("MainWindow", "12"))
        self.checkBox_max.setText(_translate("MainWindow", "ماکسیمم"))
        self.checkBox_min.setText(_translate("MainWindow", "مینیمم"))

    def dateErrorMessage(self):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        msg.setText("خطا در مقادیر تاریخ")
        msg.setInformativeText('تاریخ شروع بزرگتر از تاریخ پایان وارد  شده است.')
        msg.setWindowTitle("Error")
        msg.setStyleSheet("QLabel{min-width: 180px;}");
        msg.exec_()
        
    def sqlErrorMessage(self):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        msg.setText("خطا در برقراری ارتباط با پایگاه داده")
        # msg.setInformativeText('تاریخ شروع بزرگتر از تاریخ پایان وارد  شده است.')
        msg.setWindowTitle("Error")
        msg.setStyleSheet("QLabel{min-width: 180px;}");
        msg.exec_()
        
    def draw_plot(self):
        try:
            
            
            self . MplWidget . canvas . axes . clear () 
            
            if self.checkBox_max.isChecked():
                self . MplWidget . canvas . axes . plot ( self.time ,  self.max_total, label='max' ) 
            if self.checkBox_min.isChecked():
                self . MplWidget . canvas . axes . plot ( self.time ,  self.min_total, label='min' ) 
            
            self . MplWidget . canvas . axes. legend (( 'min' ,  'max' ), loc = 'upper right' ) 
            self . MplWidget . canvas . axes . set_title ( 'Total min/max Moisture' ) 
            self.MplWidget.canvas.axes.xaxis.set_major_locator(plt.MaxNLocator(num_of_ticks))   # sets number of ticks
            self.MplWidget.canvas.figure.autofmt_xdate()    # for readability of tick labels - it rotates them
            plt.tight_layout()
            self . MplWidget . canvas . draw ()
        
        except IndexError:
            # print('indxerror')
            pass
    
    def  update_graph ( self ):
        # start_date
        s_year = self.comboBox_year_from.currentText()
        s_month = self.comboBox_month_from.currentIndex()+1
        s_day = self.spinBox_day_from.text()
        s_time = self.timeEdit_from.time()
        start_date = JalaliDateTime(int(s_year), s_month, int(s_day), s_time.hour(), 
                                    s_time.minute(), s_time.second()).to_gregorian()
        
        # end_date
        e_year = self.comboBox_year_to.currentText()
        e_month = self.comboBox_month_to.currentIndex()+1
        e_day = self.spinBox_day_to.text()
        e_time = self.timeEdit_to.time()
        end_date = JalaliDateTime(int(e_year), e_month, int(e_day), e_time.hour(), 
                                    e_time.minute(), e_time.second()).to_gregorian()
        
        if end_date < start_date:
            self.dateErrorMessage()
            return
        
        coefficient = self.comboBox_sheet_coeficiant.currentIndex()
        if coefficient == 1:
            coefficient = 0.8
        elif coefficient == 2:
            coefficient = 1
        elif coefficient == 3:
            coefficient = 1.2
            
        try:
            connection = psycopg2.connect(user = sql_user,
                                          password = sql_pass, 
                                          host = sql_host,
                                          port = sql_port,
                                          database = db_name)
            cursor = connection.cursor()
            
            if coefficient == 0:
                q = '''SELECT min_total, max_total, date_time from moisture where 
                        date_time between TIMESTAMP %s and TIMESTAMP %s ORDER BY id;'''
                cursor.execute(q, (start_date, end_date))
            else:
                q = '''SELECT min_total, max_total, date_time from moisture where 
                        date_time between TIMESTAMP %s and TIMESTAMP %s 
                        and sheet_coefficient=%s ORDER BY id;'''
                cursor.execute(q, (start_date, end_date, coefficient))

            records = cursor.fetchall()
            
            self.min_total = [x[0] for x in records]
            self.max_total = [x[1] for x in records]
            self.time = [JalaliDateTime.to_jalali(x[2]).strftime("%Y-%m-%d\n%H:%M:%S")  for x in records]
            
            # call draw_plot function
            self.draw_plot()
                       

        except (psycopg2.Error) as error :
            if(connection):
                self.sqlErrorMessage()
        finally:
            if(connection):
                cursor.close()
                connection.close()

    def fill_year_combo(self):
        try:
            connection = psycopg2.connect(user = sql_user,
                                          password = sql_pass, 
                                          host = sql_host,
                                          port = sql_port,
                                          database = db_name)
            cursor = connection.cursor()
            
            # get date of the first record to get its year           
            q = 'SELECT date_time FROM moisture ORDER BY id ASC LIMIT 1;'
            cursor.execute(q)
            record = cursor.fetchall()      # output type is: list of tuples  
            
            start_year = JalaliDateTime.to_jalali(record[0][0]).year
            end_year = JalaliDateTime.to_jalali(datetime.datetime.now()).year
            
            # fill the comboBoxes with start and end date
            for y in range(start_year, end_year+1):
                self.comboBox_year_from.addItem( str(y) )
                self.comboBox_year_to.addItem( str(y) )
        
        except (Exception, psycopg2.Error) as error :
            if(connection):
                self.sqlErrorMessage()
                # print("Failed to insert record into mobile table", error)
        finally:
            if(connection):
                cursor.close()
                connection.close()


