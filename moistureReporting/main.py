from  PySide2.QtWidgets  import * 
from  PySide2.QtUiTools  import  QUiLoader 
from PySide2 import QtXml
from  PySide2.QtCore  import  QFile
from  PySide2 import QtGui

from  matplotlib.backends.backend_qt5agg  import  ( 
        FigureCanvas ,  NavigationToolbar2QT  as  NavigationToolbar )

# from  matplotlib.figure  import  Figure
from openpyxl import load_workbook
from openpyxl.workbook import Workbook
import os
import os.path
import datetime 
import psycopg2
from persiantools.jdatetime import JalaliDateTime
import  numpy  as  np 

from plot1 import Ui_MainWindow  as Ui_MainWindow1
from plot2 import Ui_MainWindow  as Ui_MainWindow2
from plot3 import Ui_MainWindow  as Ui_MainWindow3
from plot4 import Ui_MainWindow  as Ui_MainWindow4
from plot5 import Ui_MainWindow  as Ui_MainWindow5
from plot6 import Ui_MainWindow  as Ui_MainWindow6
from plot7 import Ui_MainWindow  as Ui_MainWindow7
from plot8 import Ui_MainWindow  as Ui_MainWindow8
from plot9 import Ui_MainWindow  as Ui_MainWindow9
from plot10 import Ui_MainWindow  as Ui_MainWindow10
from plot11 import Ui_MainWindow  as Ui_MainWindow11
from plot12 import Ui_MainWindow  as Ui_MainWindow12

sql_user = "postgres"
sql_pass = "somepass"
sql_host = "localhost"
sql_port = "5432"
db_name = "moisturemeter"


# ------------------ MainWidget ------------------ 
class  MainWidget ( QWidget ):
    
    def  __init__ ( self ):
        
        self.row = 16    # number of channels (sensors)
        self.col = 20    # length of channels
        
        QWidget . __init__ ( self )

        designer_file  =  QFile ( "Required_files/main_widget.ui" ) 
        designer_file . open ( QFile . ReadOnly )
        
        loader  =  QUiLoader () 
        self . ui  =  loader . load ( designer_file ,  self )
        designer_file . close ()
        
        self . setWindowTitle ( "گزارشگیری رطوبت سنج" )
        self.setWindowIcon(QtGui.QIcon('Required_files/chart.png'))
        
        self.fill_year_combo()
        self . ui . pushButton_open_plt1 . clicked . connect ( self . open_window1 )
        self . ui . pushButton_open_plt2 . clicked . connect ( self . open_window2 )
        self . ui . pushButton_open_plt3 . clicked . connect ( self . open_window3 )
        self . ui . pushButton_open_plt4 . clicked . connect ( self . open_window4 )
        self . ui . pushButton_open_plt5 . clicked . connect ( self . open_window5 )
        self . ui . pushButton_open_plt6 . clicked . connect ( self . open_window6 )
        self . ui . pushButton_open_plt7 . clicked . connect ( self . open_window7 )
        self . ui . pushButton_open_plt8 . clicked . connect ( self . open_window8 )
        self . ui . pushButton_open_plt9 . clicked . connect ( self . open_window9 )
        self . ui . pushButton_open_plt10 . clicked . connect ( self . open_window10 )
        self . ui . pushButton_open_plt11 . clicked . connect ( self . open_window11 )
        self . ui . pushButton_open_plt12 . clicked . connect ( self . open_window12 )
        self.ui.pushButton_saveExcel.clicked.connect(self.saveExcel)
        
        grid_layout  =  QGridLayout () 
        grid_layout . addWidget ( self . ui ) 
        self . setLayout ( grid_layout )
        
    def  open_window1 ( self ):        
        self.window = QMainWindow()
        self.ui = Ui_MainWindow1()
        self.ui.setupUi(self.window)
        self.window.show()

    def  open_window2 ( self ):
        self.window = QMainWindow()
        self.ui = Ui_MainWindow2()
        self.ui.setupUi(self.window)
        self.window.show()
        
    def  open_window3( self ):
        self.window = QMainWindow()
        self.ui = Ui_MainWindow3()
        self.ui.setupUi(self.window)
        self.window.show()
        
    def  open_window4 ( self ):
        self.window = QMainWindow()
        self.ui = Ui_MainWindow4()
        self.ui.setupUi(self.window)
        self.window.show()
        
    def  open_window5 ( self ):
        self.window = QMainWindow()
        self.ui = Ui_MainWindow5()
        self.ui.setupUi(self.window)
        self.window.show()
        
    def  open_window6 ( self ):
        self.window = QMainWindow()
        self.ui = Ui_MainWindow6()
        self.ui.setupUi(self.window)
        self.window.show()
        
    def  open_window7 ( self ):
        self.window = QMainWindow()
        self.ui = Ui_MainWindow7()
        self.ui.setupUi(self.window)
        self.window.show()
        
    def  open_window8 ( self ):
        self.window = QMainWindow()
        self.ui = Ui_MainWindow8()
        self.ui.setupUi(self.window)
        self.window.show()
        
    def  open_window9 ( self ):
        self.window = QMainWindow()
        self.ui = Ui_MainWindow9()
        self.ui.setupUi(self.window)
        self.window.show()
        
    def  open_window10 ( self ):
        self.window = QMainWindow()
        self.ui = Ui_MainWindow10()
        self.ui.setupUi(self.window)
        self.window.show()
        
    def  open_window11 ( self ):
        self.window = QMainWindow()
        self.ui = Ui_MainWindow11()
        self.ui.setupUi(self.window)
        self.window.show()
        
    def  open_window12 ( self ):
        self.window = QMainWindow()
        self.ui = Ui_MainWindow12()
        self.ui.setupUi(self.window)
        self.window.show()

    def saveExcel(self):
                
        # start_date
        s_year = self.ui.comboBox_year_from.currentText()
        s_month = self.ui.comboBox_month_from.currentIndex()+1
        s_day = self.ui.spinBox_day_from.text()
        s_time = self.ui.timeEdit_from.time()
        start_date = JalaliDateTime(int(s_year), s_month, int(s_day), s_time.hour(), 
                                    s_time.minute(), s_time.second()).to_gregorian()
        
        # end_date
        e_year = self.ui.comboBox_year_to.currentText()
        e_month = self.ui.comboBox_month_to.currentIndex()+1
        e_day = self.ui.spinBox_day_to.text()
        e_time = self.ui.timeEdit_to.time()
        end_date = JalaliDateTime(int(e_year), e_month, int(e_day), e_time.hour(), 
                                    e_time.minute(), e_time.second()).to_gregorian()
        
        if end_date < start_date:
            self.dateErrorMessage()
            return
        
        try:
            connection = psycopg2.connect(user = sql_user,
                                          password = sql_pass, 
                                          host = sql_host,
                                          port = sql_port,
                                          database = db_name)
            cursor = connection.cursor()
            
            q1 = '''SELECT date_time, tab, sheet_coefficient, avg_l, avg_r, avg_total, 
                    min_l, max_l, min_r, max_r, min_total, max_total FROM  moisture 
                    WHERE date_time between TIMESTAMP %s and TIMESTAMP %s ORDER BY id ;'''
            cursor.execute(q1, (start_date, end_date))
            records = cursor.fetchall()
            
            column_names = [x[0] for x in cursor.description]
            
            # add headers for avg_ch
            for i in range(self.row):
                column_names.append('avg_ch'+str(i+1))
            
            # add headers for min_ch
            for i in range(self.row):
                column_names.append('min_ch'+str(i+1))
            
            # add headers for max_ch
            for i in range(self.row):
                column_names.append('max_ch'+str(i+1))
            
            # add headers for cells of hum 
            for i in range(self.row * self.col):
                column_names.append('c'+str(i+1))
                
            q2 = '''SELECT  hum, avg_ch, min_ch, max_ch FROM  moisture WHERE 
                    date_time between TIMESTAMP %s and TIMESTAMP %s ORDER BY id ;'''
            cursor.execute(q2, (start_date, end_date))
            json_records = cursor.fetchall()
            
            for i in range(len(records)):
                
                # append avg_ch 
                avg = np.asarray(json_records[i][1])
                avg = tuple(avg.flatten())
                records[i] = records[i] + avg
                # append min_ch
                min = np.asarray(json_records[i][2])
                min = tuple(min.flatten())
                records[i] = records[i] + min
                # append max_ch
                max = np.asarray(json_records[i][3])
                max = tuple(max.flatten())
                records[i] = records[i] + max            
                
                # append hum cells to row
                hum = np.asarray(json_records[i][0])
                hum = tuple(hum.flatten())  # from [20*25] to [500,]
                records[i] = records[i] + hum
                
            
            currentDT = datetime.datetime.now().strftime("%Y-%m-%d-%H.%M")
            name = "moisturemeter_report_" + currentDT
            # options = QFileDialog.Options()
            # options |= QFileDialog.DontUseNativeDialog
            fileName, _ = QFileDialog.getSaveFileName(self, "Save File", name, "Excel Files (*.xlsx)")
            
            if fileName:                
                wb = Workbook()
                ws = wb.active
                ws.title = 'moisture_report'
                ws.append(column_names)
                for row in records:
                    ws.append(row)
                
                # page = wb.active
                # page.append(headers) # write the headers to the first line
                wb.save(filename = fileName)
                
                # open the file!
                
        except:
            # log the error
            pass
                
        finally:
            pass    
    
    # def saveExcel(self):
        # pass
        
    def dateErrorMessage(self):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        msg.setText("خطا در مقادیر تاریخ")
        msg.setInformativeText('تاریخ شروع بزرگتر از تاریخ پایان وارد  شده است.')
        msg.setWindowTitle("Error")
        msg.setStyleSheet("QLabel{min-width: 180px;}");
        msg.exec_()    
        
    def fill_year_combo(self):
        try:
            connection = psycopg2.connect(user = sql_user,
                                          password = sql_pass, 
                                          host = sql_host,
                                          port = sql_port,
                                          database = db_name)
            cursor = connection.cursor()
            
            # get date of the first record to get its year           
            q = 'SELECT date_time FROM moisture ORDER BY id ASC LIMIT 1;'
            cursor.execute(q)
            record = cursor.fetchall()      # output type is: list of tuples  
            
            start_year = JalaliDateTime.to_jalali(record[0][0]).year
            end_year = JalaliDateTime.to_jalali(datetime.datetime.now()).year
            
            # fill the comboBoxes with start and end date
            for y in range(start_year, end_year+1):
                self.ui.comboBox_year_from.addItem( str(y) )
                self.ui.comboBox_year_to.addItem( str(y) )
        
        except (psycopg2.Error) as error :
            if(connection):
                self.sqlErrorMessage()
        finally:
            if(connection):
                cursor.close()
                connection.close()

app  =  QApplication ([]) 
window  =  MainWidget () 
window . show () 
app . exec_ ()

### color list of matplotlib
### https://matplotlib.org/3.1.0/gallery/color/named_colors.html