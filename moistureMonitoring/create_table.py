''' This scripts is written for the purpose of initializing the system.
    It creates the database and the corresponding tables.
'''

import psycopg2
import sys

sql_user = "postgres"
sql_pass = "somepass"
sql_host = "localhost"
sql_port = "5432"
db_name = "moisturemeter"

try:
    con = psycopg2.connect(user = sql_user,
                                  password = sql_pass, 
                                  host = sql_host,
                                  port = sql_port)
    con.autocommit = True
    cur = con.cursor()
    cur.execute('CREATE DATABASE {};'.format(db_name))
    print('Database created.')
    
except (Exception, psycopg2.Error) as error :
        if(con):
            print("Failed -", error)
            input("Please press [enter] to exit.")
            sys.exit()



try:
    connection = psycopg2.connect(user = sql_user,
                                  password = sql_pass, 
                                  host = sql_host,
                                  port = sql_port,
                                  database = db_name)
    cursor = connection.cursor()
    
    q = ''' CREATE TABLE moisture(
                            id SERIAL PRIMARY KEY,
                            date_time timestamp without time zone,
                            tab smallint,
                            hum json,
                            avg_ch json,
                            avg_l numeric,
                            avg_r numeric,
                            avg_total numeric,
                            outlier_l boolean,
                            outlier_r boolean,
                            sheet_coefficient smallint,
                            min_l numeric,
                            max_l numeric,
                            min_r numeric,
                            max_r numeric,
                            min_total numeric,
                            max_total numeric,
                            min_ch json,
                            max_ch json
                            ) '''
    
    cursor.execute(q)
    connection.commit()
    
    q = ''' CREATE TABLE moisture_shadow(
                            id SERIAL PRIMARY KEY,
                            date_time timestamp without time zone,
                            tab smallint,
                            hum json,
                            avg_ch json,
                            avg_l numeric,
                            avg_r numeric,
                            avg_total numeric,
                            outlier_l boolean,
                            outlier_r boolean,
                            sheet_coefficient smallint,
                            min_l numeric,
                            max_l numeric,
                            min_r numeric,
                            max_r numeric,
                            min_total numeric,
                            max_total numeric,
                            min_ch json,
                            max_ch json,
                            copied boolean,
                            tab_id integer
                            ) '''
    
    
    cursor.execute(q)
    connection.commit()
    
    print('Two tables created.')
    
except (Exception, psycopg2.Error) as error :
        if(connection):
            print("Failed to create one of both tables the database. - ", error)
            input("Please press [enter] to exit.")
            sys.exit()

input("Please press [enter] to exit.")

    
