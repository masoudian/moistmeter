import matplotlib.pyplot as plt
import numpy as np 
import os
import socket
import sys
import time

''' waits for new data to arive from socket s, timeout = 5 min    
'''
def recv_timeout(s, timeout=300):
    chunks = []
    bytes_recd = 0
    begin = time.time()
    
    while bytes_recd < 1000:    # waiting for the data .. 
        # check if timeout
        if time.time()-begin > timeout:
            print()
            print('TimeoutError: No data received. ')
            input("Please press [enter] to exit the program and run 'cal_high.exe' again.")
            time.sleep(1)
            s.close()
            sys.exit()
            
        # check if data available & receiving data chunk by chunk until it is BUFFER_SIZE (1000) bytes.
        try:
            chunk = s.recv(min(BUFFER_SIZE - bytes_recd, BUFFER_SIZE))
            if chunk == b'':
                raise RuntimeError("socket connection broken")
            elif chunk:
                chunks.append(chunk)
                bytes_recd = bytes_recd + len(chunk)
            else:
                time.sleep(0.1)
        except:
            pass
    data = b''.join(chunks)
    print('Data received.')
    return data


''' main block 
    receives data from sensors, 
    then reformats and saves it to a file
'''

IP = '192.168.1.150'
PORT = 8080
BUFFER_SIZE = 1000
TIMEOUT = 300

input("Please put the caps on the sensors,  then press [enter] to continue.")
print()

try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print('Socket created')    
except socket.error:
    print('Failed to create socket')
    input("Please press [enter] to exit the program and run 'cal_high.exe' again.")
    time.sleep(1)
    s.close()
    sys.exit()

try:
    s.connect((IP, PORT))
    print('Connected to the socket.')
except:
    print()
    print('ConnectionTimeoutError: Make sure you are connected to the socket.')
    input("Please press [enter] to exit the program and run 'cal_high.exe' again.")
    time.sleep(1)
    s.close()
    sys.exit()
    
s.setblocking(0) 

time.sleep(0.2)
s.send(bytearray([67, 65, 76, 49, 10]))
print('Signal sent, waiting for the sensors to read data...')

data = recv_timeout(s, TIMEOUT)

# convert 1000 byte to a 500 unsigned integers vector
int_vec = [int.from_bytes(data[i:i+2], byteorder='big') for i in range(0, len(data), 2)]
# make a 2d array[25,20] from vector 
int_arr = [int_vec[i:i+20] for i in range(0, len(int_vec), 20)]
# rotate the array => 20*25
int_arr = list(map(list, zip(*int_arr)))

tab = int.from_bytes(s.recv(1), byteorder='big')

datah = np.mean(int_arr,axis=1, keepdims=1) * np.ones([1,25])
# save it to the file
if not os.path.exists('Required_files'):
    os.makedirs('Required_files')
np.savetxt("Required_files/datah.txt", datah, fmt="%s")

# draw the plot
plt.imshow(int_arr, cmap=plt.cm.jet)
labels = range(0, 20, 2)
plt.yticks(labels)
plt.colorbar()
plt.clim(4000,7500)
plt.show()

time.sleep(0.2)
s.send(bytearray([82, 65, 68, 49, 10]))

s.close()
print()
print('SUCCESS, "datah.txt" is generated.')
input("Please press [enter] to exit the program.")
print('Socket closed.')
print('Closing the program.')
time.sleep(1)
