''' multithread version - plot and show with pyplot '''

import datetime
import json
import matplotlib.pyplot as plt
import msvcrt
import numpy as np 
from openpyxl import load_workbook
from openpyxl.workbook import Workbook
import os
import os.path
import psycopg2
import socket
import sys
import threading
import time

PORT = 8080
IP = '192.168.1.150'    # default IP adress
BUFFER_SIZE = 1000
TIMEOUT = 1200
input_timeout = 5       # timeout for inserting new IP address
max_plot_index = 1000   # saves upto max_plot_index plots
sheet_coefficient = 1   # thickness of sheets

device_row = 20
device_col = 25

row = 20    # number of active channels
col = 25    # length of pannel
min_limit = -5
max_limit = 25

sql_user = "postgres"
sql_pass = "somepass" 
sql_host = "localhost"
sql_port = "5432"
db_name = "moisturemeter"

excel_directory_name = 'Runs/ExcelFiles/'
plot_directory_name = 'Runs/PlotFiles/'
# exec_path = "Required_files/geckodriver.exe"     # path for firefox driver
IP_Addr_file_path = 'Required_files/IP_Addr_file'
last_index_file_path = 'Required_files/last_index'
avg_ch_limit_path = 'Required_files/avg_ch_limit'
avg_total_limit_path = 'Required_files/avg_total_limit'

data = np.zeros((row,col))    # shared variable between threads - filled with data from socket
tab = 0                     # tab number - shared variable between threads 
data_flag = False   # it is used to signal plotting thread that there is new data available
stop_flag = False   # it is used to signal all threads to terminate themselves
# outlier_l_flag = False  
# outlier_r_flag = False  # if there's an outlier in a right panel cell, set to true, and calculate avg_r again
ave = np.zeros([8])
lock = threading.Lock()


''' re-initialize the socket '''
def open_socket():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error:
        print('\nFailed to create socket.')
        input("Please press [enter] to exit.")
        time.sleep(1)
        update_stop_flag()

    try:
        s.connect((IP, PORT))
        print('Connected to the socket.')
    except:
        print('\nConnectionTimeoutError: Make sure you are connected to the socket.')
        input("Please press [enter] to exit the program and run 'Moisture_Meter.exe' again.")
        time.sleep(1)
        update_stop_flag()

    s.setblocking(0)

''' checks previous file index - for saving 1000 plot figures'''
def get_new_index():
    file_name = last_index_file_path
    if os.path.exists(file_name):
        f = open(file_name, "r")
        last_index = f.read()
        if last_index == '':
            new_index = 1
        elif int(last_index) < max_plot_index:
            new_index = int(last_index) + 1
        else:
            new_index = 1
    else:   # last_index file does not exist; create an empty file and return index = 1
        file = open(file_name, "w")
        file.close()
        new_index = 1
    return str(new_index)

''' generates new file name = currentDT_index '''
def get_file_name(index):
    currentDT = datetime.datetime.now().strftime("%Y-%m-%d,%H.%M.%S")
    name = currentDT + '_' + index + '.png'
    return name

''' updates the last_index file - writes the latest index '''
def update_last_index(new_index):
    file_name = last_index_file_path
    file = open(file_name, "w")
    file.write(new_index)
    file.close()
    
''' deletes the image if the new index is the same as an existing image '''
def delete_same_index_file(index):
    for file in os.listdir(plot_directory_name):
        if file.endswith("_"+index+'.png'):
            os.remove(plot_directory_name + file)

''' create a new excel file and initiate the headers '''
def new_excel_file():
    currentDT = datetime.datetime.now().strftime("%Y-%m-%d,%H.%M.%S")
    workbook_name = currentDT+'.xlsx'
    headers = ['','L1','R1','L2','R2','L3','R3','L4','R4']
    wb = Workbook()
    page = wb.active
    page.append(headers) # write the headers to the first line
    wb.save(filename = excel_directory_name + workbook_name)
    return workbook_name

''' sets stop_flag to true '''
def update_stop_flag():
    global stop_flag
    lock.acquire()
    stop_flag = True
    lock.release()
    
''' receive function with timeout
    waits for new data to arive from socket s, timeout = 20 min '''   
def recv_timeout(s, timeout):
    chunks = []
    bytes_recd = 0
    begin = time.time()
    
    while bytes_recd < 1000:    # waiting for the data .. 
        # check if timeout
        if time.time()-begin > timeout:
            print()
            print('Timeout: No data received, Program paused ')
            print('Please press "c" to Continue, ')
            print('or "r" to restart the program,')
            print('or "Esc" to exit the program.')
            print()
            while True:
                if msvcrt.kbhit():          
                    key = ord(msvcrt.getch())
                    if key == 27: # Esc.       
                        print('Exiting..')
                        time.sleep(2)
                        update_stop_flag()
                        s.close()
                        return 0
                    if key == 99 or key == 67:     # c or C
                        begin = time.time()
                        print()
                        print('Continueing the program.')
                        print()
                        break
                    elif key == 114 or key == 82:        # r or R
                        s.close()
                        print()
                        open_socket()
                        print('The program was restarted.')
                        print()
                        # print('Waiting for the signal and data..')
                        begin = time.time()
                        # breaks from the current while loop, and waits for data in the upper loop
                        break
        
        # check if data is available & receive data chunk by chunk until it gets BUFFER_SIZE (1000) bytes.
        try:
            chunk = s.recv(min(BUFFER_SIZE - bytes_recd, BUFFER_SIZE))
            if chunk == b'':
                raise RuntimeError("socket connection broken")
            elif chunk:
                chunks.append(chunk)
                bytes_recd = bytes_recd + len(chunk)
            else:
                time.sleep(0.1)
        except:
            pass
    data = b''.join(chunks)
    # print('Data received.')
    return data

''' validates user input to see if it is a valid ip '''
def validate_ip(s):
    a = s.split('.')
    if len(a) != 4:
        return False
    for x in a:
        if not x.isdigit():
            return False
        i = int(x)
        if i < 0 or i > 255:
            return False
    return True


''' thread related function
    upadate `data` when we have new `hum` & update new `tab` number
    tab & data are the shared variable between threads
'''
def update_data_sync(hum, t):
    global tab
    global data
    global data_flag
    lock.acquire()
    data = np.copy(hum)
    data_flag = True
    tab = t
    lock.release()

''' plotting thread uses this function to access the new data '''
def get_data_sync():
    global data_flag
    lock.acquire()
    d = np.copy(data)
    data_flag = False
    lock.release()
    return (d)

''' plotting thread uses this function to access tab variable '''
def get_tab_sync():
    lock.acquire()
    t = np.copy(tab)
    lock.release()
    return (t)

''' gets tab number of the last record of moisture_shadow table '''
def get_last_tab():
    try:
        connection = psycopg2.connect(user = sql_user,
                                      password = sql_pass,
                                      host = sql_host,
                                      port = sql_port,
                                      database = db_name)
        cursor = connection.cursor()
        query = "SELECT tab, tab_id FROM moisture_shadow ORDER BY id DESC LIMIT 1"
        cursor.execute(query)
        record = cursor.fetchall()
        if len(record) == 0:     # database is empty
            return None
        return record[0]
        
    except (Exception, psycopg2.Error) as error :
        if(connection):
            print("Failed to insert record into the database. - ", error)
            update_stop_flag()
    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()


def detect_outlier(hum):
    pass

            
''' claculate the average and min/max values of hum
    insert the values to database tables (moisture & moisture_shadow)
    and save data into excel file
'''
def insert_data(hum, tab, avg_ch_limit, avg_total_limit, arrival_time, workbook_name):
    global ave
    outlier_l_flag = False  # if there's an outlier in a left panel cell, set to true, and recalculate avg_l
    outlier_r_flag = False
    
    min_l = np.amin(hum[0:int(row/2)])   # [0:8]
    min_r = np.amin(hum[int(row/2):row]) # [8:16]
    
    max_l = np.amax(hum[0:int(row/2)])
    max_r = np.amax(hum[int(row/2):row])
    
    min_total = np.minimum(min_l, min_r)
    max_total = np.maximum(max_l, max_r)
    
    min_ch = np.amin(hum, axis=1)
    max_ch = np.amax(hum, axis=1)
    
    avg_l = hum[0:int(row/2)].mean()
    avg_r = hum[int(row/2):row].mean()
    avg_total = (avg_l + avg_r)/2
    
    avg_ch = hum.mean(axis=1)
        
    # check for warnings!
    for i in range(0,row):
        # print(type(i))
        if avg_ch[i] > float(avg_ch_limit):
            print('WARNING! Average moisture out of normal range (!', +str(avg_ch_limit)+')', 
                    '\nChannel number:', i+1, '\tValue:', avg_ch[i], '\tGenerated at', arrival_time )
    
    if avg_total > float(avg_total_limit):
        print('WARNING! Total average moisture out of normal range('+str(avg_total_limit)+').', 
              '\nTotal average moisture:', avg_total, '\tValue', '\tGenerated at', arrival_time)
    
    # insert the record into the tables ######################################################
    previous_tab = get_last_tab()   # init prev_tab values (tab and tab_id)
    if previous_tab is not None:
        prev_tab = previous_tab[0]
        tab_id = previous_tab[1]
        if prev_tab == 4:   # if prev tab is 4 then update tab_id by one
            tab_id += 1
        
    try:
        connection = psycopg2.connect(user = sql_user,
                                      password = sql_pass,
                                      host = sql_host,
                                      port = sql_port,
                                      database = db_name)
        cursor = connection.cursor()
        
        ##### insert into moisture
        query = """ INSERT INTO moisture (date_time, tab, hum, avg_ch, avg_l, avg_r, 
                    avg_total, min_l, max_l, min_r, max_r, min_total, max_total, min_ch, 
                    max_ch, outlier_l, outlier_r, sheet_coefficient) 
                    VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) """
        values = (arrival_time, tab, json.dumps(hum.tolist()), json.dumps(avg_ch.tolist()), 
                  avg_l, avg_r, avg_total, min_l, max_l, min_r, max_r, min_total, max_total, 
                  json.dumps(min_ch.tolist()), json.dumps(max_ch.tolist()), outlier_l_flag, 
                  outlier_r_flag, sheet_coefficient)
        cursor.execute(query, values)
        connection.commit()
        
        ##### insert into moisture_shadow
        query = """ INSERT INTO moisture_shadow (date_time, tab, hum, avg_ch, avg_l, avg_r, 
                    avg_total, min_l, max_l, min_r, max_r, min_total, max_total, min_ch, 
                    max_ch, outlier_l, outlier_r, sheet_coefficient, copied, tab_id) 
                    VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) """
        
        if previous_tab is None:
            # if this is the first insert to db and tab != 1
            # then insert new data to shadow ( * tab-1 times ) 
            # tab-1 because there is one more insert bellow, after this if
            tab_id = 1
            if tab != 1:
                prev_tab = 1
                while tab != prev_tab:
                    values = (arrival_time, prev_tab, json.dumps(hum.tolist()), json.dumps(avg_ch.tolist()), 
                              avg_l, avg_r, avg_total, min_l, max_l, min_r, max_r, min_total, max_total, 
                              json.dumps(min_ch.tolist()), json.dumps(max_ch.tolist()), outlier_l_flag, 
                              outlier_r_flag, sheet_coefficient, True, tab_id)
                    cursor.execute(query, values)
                    connection.commit()
                    prev_tab += 1
                    
        # check if new tab is sequential to prev_tab - if not get four prev records from 
        # shadow table to insert missing tab with prev data that has the same tab number
        elif tab != prev_tab % 4 + 1:
            q = """SELECT date_time, tab, hum, avg_ch, avg_l, avg_r, avg_total, 
                    min_l, max_l, min_r, max_r, min_total, max_total, min_ch, 
                    max_ch, outlier_l, outlier_r, sheet_coefficient, copied, tab_id
                    FROM moisture_shadow ORDER BY id DESC LIMIT 4"""
            cursor.execute(q)
            records = cursor.fetchall() # it is a list of tuples
            
            # duplicate previous data into shadow until prev_tab+1 == tab
            while tab != prev_tab % 4 + 1:
                for r in records:
                    if r[1] == prev_tab % 4 + 1:  
                        # insert r as the missing tab data
                        r = list(r)               # convert it to a list to be able to change the values
                        r[2] = json.dumps(r[2])   # hum - type is list - convert to json
                        r[3] = json.dumps(r[3])   # avg_ch
                        r[13] = json.dumps(r[13]) # min_ch
                        r[14] = json.dumps(r[14]) # max_ch
                        r[18] = True              # set copied field to true
                        # r[1] = prev_tab % 4 + 1   # update tab value by one
                        r[19] = tab_id            # set tab_id
                        
                        cursor.execute(query, r)
                        connection.commit()
                        prev_tab = prev_tab % 4 + 1     # update prev_tab by one
                        
                        if prev_tab == 4: 
                            tab_id += 1
                            
        # here save the current data to shadow table too.
        values = (arrival_time, tab, json.dumps(hum.tolist()), json.dumps(avg_ch.tolist()), 
                  avg_l, avg_r, avg_total, min_l, max_l, min_r, max_r, min_total, max_total, 
                  json.dumps(min_ch.tolist()), json.dumps(max_ch.tolist()), outlier_l_flag, 
                  outlier_r_flag, sheet_coefficient, False, tab_id)
        cursor.execute(query, values)
        connection.commit()
        
        prev_tab = tab
        if prev_tab == 4:
            tab_id += 1
        
    except (Exception, psycopg2.Error) as error :
        if(connection):
            print("Failed to insert record into the database. - ", error)
            update_stop_flag()
            return
    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
    
    # save the info to the excel file #######################################################
    if tab>0 & tab<5:
        ave[2*tab-2] = avg_l
        ave[2*tab-1] = avg_r
    if tab == 4:
        try:
            wb = load_workbook(excel_directory_name + workbook_name)
        except FileNotFoundError:
            print()
            print('FileNotFoundError: Could not find "' + excel_directory_name + workbook_name + '".')
            input("Please press [enter] to exit the program and run 'Moisture_Meter.exe' again.")
            time.sleep(1)
            update_stop_flag()
            return
            
        page = wb.active
        row_title = [arrival_time.strftime("%Y-%m-%d,%H:%M:%S")]
        page.append(row_title + ave.tolist())
        try:
            wb.save(filename=excel_directory_name + workbook_name)
        except PermissionError:
            print('PermissionError: Could not write to the "' + excel_directory_name + workbook_name + '", probably because the file is open.')
            input("Please press [enter] to exit the program and run 'Moisture_Meter.exe' again.")
            time.sleep(1)
            update_stop_flag()
            return
        if page.max_row > 350:  
            # create a new excel file
            workbook_name = new_excel_file()
    
def set_in_range(hum):
    
    for i,j in np.ndindex(hum.shape):
        if hum[i,j] < 0:
            hum[i,j] = 0
        elif hum[i,j]>20:
            hum[i,j] = 20
    return hum


''' Here is all the code for 
    - receiving and processing the data from socket 
    - creating and updating the excel file
'''
def worker():
    global stop_flag
    # ave = np.zeros([8])
    if not os.path.exists(excel_directory_name):
        os.makedirs(excel_directory_name)

    if not os.path.exists(plot_directory_name):
        os.makedirs(plot_directory_name)

    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error:
        print('Failed to create socket.')
        input("Please press [enter] to exit.")
        time.sleep(1)
        update_stop_flag()
        return

    try:
        s.connect((IP, PORT))
        print('Connected to the socket.')
    except:
        print()
        print('ConnectionTimeoutError: Make sure you are connected to the socket.')
        input("Please press [enter] to exit the program and run 'Moisture_Meter.exe' again.")
        time.sleep(1)
        update_stop_flag()
        return
        
    s.setblocking(0)

    try:
        datal = np.genfromtxt('Required_files/datal.txt', delimiter = ' ')
    except IOError:
        print()
        print('OSError: "datal.txt" not found.') 
        print('Please make sure the specified file is in the "Required_files" directory.')
        input("Please press [enter] to exit the program and run 'Moisture_Meter.exe' again.")
        time.sleep(1)
        update_stop_flag()
        return
    try:
        datah = np.genfromtxt('Required_files/datah.txt', delimiter = ' ')
    except IOError:
        print()
        print('IOError: "datah.txt" not found.')
        print('Please make sure the specified file is in the "Required_files" directory.')
        input("Please press [enter] to exit the program and run 'Moisture_Meter.exe' again.")
        time.sleep(1)
        update_stop_flag()
        return
    
    try:
        f = open(avg_ch_limit_path, "r")
        avg_ch_limit = f.read()
    except IOError:
        print('IOError: "avg_ch_limit" file not found.')
        print('Please make sure the specified file is in the "Required_files" directory.')
        input("Please press [enter] to exit the program and run 'Moisture_Meter.exe' again.")
        time.sleep(1)
        update_stop_flag()
        return
        
    try:
        f = open(avg_total_limit_path, "r")
        avg_total_limit = f.read()
    except IOError:
        print('IOError: "avg_total_limit" file not found.')
        print('Please make sure the specified file is in the "Required_files" directory.')
        input("Please press [enter] to exit the program and run 'Moisture_Meter.exe' again.")
        time.sleep(1)
        update_stop_flag()
        return
            
            
    # initialize a new excel file
    workbook_name = new_excel_file()
    
    # remove unwanted cells (we'll keep rows 2 to 17 and columns 0 to 19) => [16*20]
    # datal = np.delete(datal, [0,1,18,19], 0)
    # datal = np.delete(datal, [20,21,22,23,24], 1)
    # datah = np.delete(datah, [0,1,18,19], 0)
    # datah = np.delete(datah, [20,21,22,23,24], 1)
    
    
    while True:
        # print('Waiting for the signal and data..')
        raw_data = recv_timeout(s, TIMEOUT)
        
        # arrival time of new data
        arrival_time = datetime.datetime.now()
        
        if stop_flag == True:   # user hit 'esc' to exit
            break
        
        # convert 1000 byte to a 500 unsigned integers vector
        int_vec = [int.from_bytes(raw_data[i:i+2], byteorder='big') for i in range(0, len(raw_data), 2)]
        # make a 2d array[25,20] from vector 
        int_arr = [int_vec[i:i+device_row] for i in range(0, len(int_vec), device_row)]
        # rotate the array => 20*25
        int_arr = list(map(list, zip(*int_arr)))
        
        # remove unwanted cells (we'll keep rows 2 to 17 and columns 0 to 19) => [16*20]
        # int_arr = np.delete(int_arr, [0,1,18,19], 0)
        # int_arr = np.delete(int_arr, [20,21,22,23,24], 1)
        
        
        tab = int.from_bytes(s.recv(1), byteorder='big')
        hum = (datal-int_arr)/(datal-datah)*10*sheet_coefficient            
        
        
        # hum = set_in_range(hum)
        
        # copy hum to the shared variable & set flag true to signal plot_thread + update tab variable
        update_data_sync(hum, tab)
        
        # call function to calculate min/max/avg vals and insert into db
        insert_data(hum, tab, avg_ch_limit, avg_total_limit, arrival_time, workbook_name)
        
        
####################################### MAIN SECTION ##########################################
print('Welcome! \n')
print()

''' ask user to insert new ip '''
print('Change the IP address (y/n)? ')
print('Type "y" to change the IP, or "n" to continue.')
begin = time.time()
while True:     # outer while
    if time.time()-begin > input_timeout:
        # read from IP_Addr_file (if available)
        if os.path.exists(IP_Addr_file_path):
            f = open(IP_Addr_file_path, "r")
            IP = f.read()
            print('Using ' + IP + ' IP address')
            break
        else:
            print('Using ' + IP + ' IP address')
            break
    if msvcrt.kbhit():          
        key = ord(msvcrt.getch())
        if key == 121 or key == 89: # y or Y
            ip = input('Please insert the new IP address, or insert "c" and press Enter to cancel:  ')
            while True: # inner while
                if ip == "c":
                    # read from IP_Addr_file (if available)
                    if os.path.exists(IP_Addr_file_path):
                        f = open(IP_Addr_file_path, "r")
                        IP = f.read()
                        print('Using ' + IP + ' IP address')
                        break
                    else:
                        print('Using ' + IP + ' IP address')
                        break
                else:
                    if validate_ip(ip) : # if True 
                        IP = ip
                        print('Using ' + IP + ' IP address')
                        #update ip file
                        file = open(IP_Addr_file_path, "w")
                        file.write(ip)
                        file.close()
                        break
                    else:
                        ip = input('Wrong IP address, please re-enter the correct IP, or enter "c" to cancel: ')
            break      # break from outer while
        elif key == 110 or key == 78: # n or N
            # read from IP_Addr_file (if available)
            if os.path.exists(IP_Addr_file_path):
                f = open(IP_Addr_file_path, "r")
                IP = f.read()
                print('Using ' + IP + ' IP address')
                break
            else:
                print('Using ' + IP + ' IP address')
                break
        else:
            print('Please type "y" to change the IP, or "n" to continue.')
            begin = time.time()

            
''' ask user to insert thickness of sheets '''
print('\nPlease select the Sheet Coefiicient:')
print("For coefficient of '8' insert '1',\nfor coefficient of '10' insert '2',\nand for coefficient of '12' insert '3'\nfor inserting your own coefficient insert 4:")
while True:
    user_choice = input('Sheet Coefficient: ')
    if user_choice == '1':
        sheet_coefficient = 0.8
    elif user_choice == '2':
        sheet_coefficient = 1
    elif user_choice == '3':
        sheet_coefficient = 1.2
    elif user_choice == '4':
        sheet_coefficient = int(input('Please insert the sheet coefficient: '))
    else:
        print('Please insert again, the choices are `1`, `2`, or `3`')
        continue
    #print('coef', sheet_coefficient)
    break

print()
    
''' multithreading - main thread is for plotting, other thread is for all other operations '''
if __name__ == "__main__":
    thr = threading.Thread(target=worker)
    thr.start()
    #plt.ion()
    while True:
        if stop_flag == True:   # user hit 'esc' to exit
            break
            
        if data_flag == False:  # no new data
            try:
                plt.pause(0.33)
                continue
            except:     # if no plt window is open
                continue
        # else - pick up new data & tab
        data = get_data_sync()   
        tab = get_tab_sync()
        try:
            plt.close()
        except:
            pass    # if plt window is already closed by user
        
        # left subplot
        plt.subplot(1,2,1)
        plt.suptitle(tab, size=16)
        plt.imshow(np.swapaxes(data[0:int(row/2), ::-1],0,1),
                   extent=[0,1.5,3,0], 
                   cmap=plt.cm.jet_r)
        plt.colorbar()
        plt.clim(0,15)
        plt.xlabel('width(m)')
        plt.ylabel('length(m)')
        plt.title('LEFT', size=16)
        
        # right subplot
        plt.subplot(1,2,2)
        plt.imshow(np.swapaxes(data[int(row/2):row, ::-1],0,1),
                   extent=[0,1.5,3,0], 
                   cmap=plt.cm.jet_r)
        plt.colorbar()
        plt.clim(0,15)
        plt.xlabel('width(m)')
        plt.ylabel('length(m)')
        plt.title('RIGHT', size=16)
        
        plt.subplots_adjust(hspace=0.4, wspace=0.4)
        plt.suptitle('tab='+str(tab), size=20)
                
        manager = plt.get_current_fig_manager()
        manager.window.state('zoomed')
        
        plt.show(block=False)
        plt.pause(0.00001)  # pause to be able to show the plot figure
        
        ## saving last 1000 plots
        # calculates new index from prev index
        new_index = get_new_index()
        # generate new file name
        newFileName = get_file_name(new_index)
        # deletes plot with the same index, if any
        delete_same_index_file(new_index)
        # save the plot
        plt.savefig(plot_directory_name + newFileName, dpi=500)
        # update the last index file
        update_last_index(new_index)
        
    thr.join()
    sys.exit()
    